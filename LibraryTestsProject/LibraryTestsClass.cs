﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.CSharp.RuntimeBinder;
using System.Web.Mvc;
using Task23_Advanced.Controllers;

namespace LibraryTestsProject
{
    [TestClass]
    public class LibraryTestsClass
    {
        [TestMethod]
        public void MainControllerTest_ReturnViewMustBeNotNull()
        {
            var controller = new MainController();
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void MainControllerReturnViewTest_IndexShouldReturnCorrectViewName()
        {
            var controller = new MainController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("Index",result.ViewName);
        }

        [TestMethod]
        public void GuestControllerReturnViewTest_IndexShouldReturnCorrectViewName()
        {
            var controller = new GuestController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void QuestionnaireControllerReturnViewTest_IndexShouldReturnCorrectViewName()
        {
            var controller = new QuestionnaireController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void QuestionnaireControllerReturnViewTest_IndexShouldReturnCorrectViewBag()
        {
            var controller = new QuestionnaireController();
            var result = controller.Index() as ViewResult;

            var Scales = result.ViewBag.Scales;
            Assert.AreEqual(false, Scales);
        }

    }
}
