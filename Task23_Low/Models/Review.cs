﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Task23_Advanced.Validation;

namespace Task23_Advanced.Models
{
    /// <summary>
    /// Review Model to represent reviews.
    /// </summary>
    public class Review
    {
        public Review(string AuthorName,DateTime Date,string Content)
        {
            this.AuthorName = AuthorName;
            this.Date = Date;
            this.Content = Content;
        }
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string AuthorName { get; set; }
        [Review(1999)]
        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }
        [Required]
        public string Content { get; set; }
    }
}