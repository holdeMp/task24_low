﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task23_Advanced.Models
{
    /// <summary>
    /// Questionnaire Model to represent questionnaires.
    /// </summary>
    public class Questionnaire
    {
        public Questionnaire(string FullName, string Adress, bool? Scales, bool? City, bool? Work, string Email
            , string Organisation, string Region, string Country, string Experience, string Position)
        {
            this.FullName = FullName;
            this.Adress = Adress;
            this.Scales = Scales;
            this.City = City;
            this.Work = Work;
            this.Email = Email;
            this.Organisation = Organisation;
            this.Region = Region;
            this.Country = Country;
            this.Experience = Experience;
            this.Position = Position;

        }
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string FullName { get; set; }
        public string Adress { get; set; }
        public bool? Scales { get; set; }
        public bool? Horns { get; set; }
        public bool? City { get; set; }
        public bool? Work { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string Organisation { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Experience { get; set; }
        public string Position { get; set; }
    }
}